<h2>
    This Repo has my configuration files for the following linux software programs.  
</h2>


1. `i3wm` - A simple tiling windowing manager for X11 based compisitor.
2. `Alacritty` - An OpenGL terminal emulator
3. `neovim` & `vim` - Terminal based text editors
4. `tmux` - A terminal multiplexer
5. `sway` - A simple tiling window manager like i3 for Wayland based compositor.
6. `statusbar`<sup> AUR </sup> - A statusbar tool for sway to generate a highly customizable statusbar using tools like swaybar-protocol and many more. 


<h2>
    Recommended depencencies (common dependencies)
</h2>

__( Click on the names below to download them / follow respective instructions )__

- [JetBrainsMono NERD Font](https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/JetBrainsMono.zip) - _This is a cool font for making Text look good (Used for alacritty), use tools like 'font-manager' to add this font into your system_

- [rofi-power-menu](https://gitlab.com/leolegendnarasimha/config-files/-/blob/main/binaries/rofi-power-menu) - _This tool can be used to launch drun for power menu with rofi application launcher. ( 
This link redirects to binary available in this repo itself )_

- [wofi-power-menu](https://gitlab.com/leolegendnarasimha/config-files/-/blob/main/binaries/wofi-power-menu) - _This tool is similar to rofi but used with wofi application launcher. ( This link redirects to binary available in this repo itself )_

**Note:**
    _*The additional software specific dependencies are listed in '.deps.toml' file*_.

